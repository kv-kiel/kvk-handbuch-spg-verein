= Anwendungsfälle
Mathias-H. Weber <mhw@teambaltic.de>
v1.0, 12. Oktober 2020
:doctype: article
:encoding: utf-8
:lang: de
//:toc: left
//:toclevels: 4
//:toc-title: Inhaltsverzeichnis
:last-update-label: Erstellt mit Asciidoctor v{asciidoctor-version} : Zuletzt geändert:
// Ohne dem haben die "Admonition"-Blocks keine Icons!
:icons: font
:numbered:
:source-highlighter: highlightjs
// Deutsche Überschriften:
:figure-caption: Abbildung
:table-caption: Tabelle
:chapter-label: Kapitel
//:example-caption!:
// Jeder Abschnitt bekommt automatisch einen Anker:
:sectanchors:
// Makro "kbd:" aktivieren:
:experimental:
:imagesdir: images

== Änderungen nach Mitgliederversammlung

Nach jeder Mitgliederversammlung müssen die einschlägigen Einträge der ausgeschiedenen und neu gewählten Amtsinhaber in der Mitgliederverwaltung angepasst werden.

Bei Ausscheiden aus einem Amt muss in die Spalte `AD-Frei.bis` der Monatsletzte des Monats eingetragen werden, in dem die Mitgliederversammlung stattgefunden hat.

Bei Neuwahl in ein Amt muss in der Spalte `AD-Frei.Grund` der Wert `MANAGEMENT` eingetragen werden und in die Spalte `AD-Frei.von` der Monatserste des Monats, in dem die Mitgliederversammlung stattgefunden hat.

== Neueintritt

Bei einem Neueintritt sind die im Eintrittsformular gemachten Angaben in die Vereinsverwaltung zu übertragen. 

Auf Seite eins der Mitgliederdaten ist insbesondere auf die EMail-Adresse zu achten und die korrekte Mitgliedsnummer zu einer verknüpften Person anzugeben. 

Als Anrede bei männlichen Mitgliedern ist "Herrn" auszuwählen, damit die Adressangabe in den automatisch erzeugten Briefen korrekt aufgebaut wird.

Als Eintrittsdatum sollte immer der 1. eines Monats ausgewählt werden. In der Regel wird das der 1. des Monats sein, der auf den Termin der Vorstandssitzung folgt, auf der die Aufnahme beschlossen worden ist.

Als Option für einen Eintrittsbrief ist `ja´ auszuwählen.

[IMPORTANT,caption=WICHTIG]
====
Es ist essentiell wichtig, dass für alle neu aufgenommenen Mitglieder eine EMail-Adresse vorhanden ist, sonst kommt der gesamte Ablauf des Verschickens der Eintrittsbriefe durcheinander!

Sollte ein Neumitglied keine EMail-Adresse besitzen, darf bei diesem auch die Option `Eintrittsbrief` nicht angewählt sein! 
====

image:Eintritt.png[]

Auf der Seite zwei der Mitgliederdaten ist als Zahlungsart `SEPA-Lastschrift` auszuwählen und die zugehörgien Bankdaten einzutragen.

[IMPORTANT,caption=WICHTIG]
====
Es muss unbedingt eine SEPA-Einzugsermächtigung im Aufnahmeantrag erteilt worden sein!
====

image:Eintritt2.png[]

Die SEPA-Mandatsreferenz wird gebildet auf dem Prefix `MIT-` gefolgt von der Mitgliedsnummer. Sie muss manuell eingegeben werden. Wenn man auf den Knopf mit den drei Punkten daneben klickt, wird schon mal die Mitgliedsnummer in das Feld geschrieben - eine wesentliche Hilfe. Man muss nur noch das Präfix davorsetzen.

In das Feld `vom` gibt man das Datum ein, wann der Aufnahmeantrag unterschrieben wurde.

Im Bereich `Abteilungen/Beiträge` müssen alle Beitragskomponenten eingegeben werden. Bei einem Neumitglied liegt in der Regel nur eine Komponente vor, erst wenn z.B. ein Bootsplatz zugewiesen wird, kommt dafür eine zweite Komponente hinzu. 

[IMPORTANT,caption=WICHTIG]
====
Die Zahlungsweise muss unbedingt auf `halbjährl.` gestellt werden!
====

Die Spalte `Eintritt` wird vom Programm lediglich für statistische Zwecke verwendet. Hier sollte im Normalfall einfach das Datum des Eintritts in den Verein stehen.

In das Feld `Einmalbetrag(2)`(!) ist die entsprechende Aufnahmegebühr einzutragen.

Liegt der Termin des Vereinseintritts nicht auf dem Beginn eines Halbjahres, so ist in das Feld `Erhebung ab` der Beginn des ersten vollen Halbjahres der Mitgliedschaft einzutragen. Zusätzlich ist die Beitragssumme für die Monate vor dem Beginn des Halbjahres zu berechnen und mit der Aufnahmegebühr zusammen in das Feld `Einmalbetrag(2)` einzutragen. Im Beispiel des Bildes fällt für das Mitglied der Beitrag für den Monat Juni an (12 Euro).

Bei dem Beispiel im Bild erfolgte der Eintritt zum 01.06., das Halbjahr beginnt aber erst zum 01.07. Also erfolgt die Erhebung erst ab dem 01.07. und der Beitrag für den Monat Juni, der dadurch flöten gehen würde, wird zusammen mit der Aufnahmegebühr von 25 Euro über den `Einmalbetrag(2)` eingezogen. So ergibt sich der im Bild dargestellte Betrag von 37 Euro für den `Einmalbetrag(2)`.

Hat das Mitglied bereits einen Hallenschlüssel erhalten, muss die Pfandgebühr in das Feld  `Einmalbetrag(1)` eingetragen werden.

[IMPORTANT,caption=WICHTIG]
====
Es ist wichtig, die Felder  `Einmalbetrag(1)` und  `Einmalbetrag(2)` _immer_ nur für die ihnen zugewiesene Bedeutung zu verwenden:  *`Einmalbetrag(1)`* für das Schlüsselpfand und  *`Einmalbetrag(2)`* für Aufnahmegebühr und Beiträge für die Monate, die am vollen Halbjahr fehlen. Beim SEPA-Lauf werden nämlich entsprechende Kommentare für die Bedeutung der jeweiligen Zahlungen mitgeliefert.
====

[TIP,caption=TIPP]
====
Besondere Herausforderungen stellt die Situation dar, dass nachträglich ein Partner einer bereits seit längerem als Mitglied registrierten Person eintritt.

Hier ist für den *neu eintretenden Partner* auf der ersten Seite der Mitgliedsdaten die Verknüpfung zu der bereits registrierten Person einzugeben. Auf Seite zwei der Mitgliedsdaten ist bei der Beitragsart "Partner mit (oder ohne) Boot" auszuwählen. Eine Kontoverbindung braucht nicht angegeben zu werden, weil ja das Konto der verknüpften Person genutzt wird.

Schließlich müssen dann noch die Daten der *verknüpften Person* angepasst werden. Als Beitragsart muss nun "FAM mit (oder ohne) Boot" gewählt werden. In das Feld `Einmalbetrag(2)` ist die Aufnahmegebühr für den neu eintretenden Partner einzutragen. 

Die höhere Familiengebühr wird fällig mit dem Eintrittstermin des Partners. Liegt dieser Termin nicht genau auf dem Beginn eines Halbjahres und ist der  SEPA-Lauf für das betreffende Halbjahr noch nicht getätigt, würde durch den nächsten SEPA-Lauf die Familiengebühr aber für das volle Halbjahr eingezogen werden. Daher ist die Aufnahmegebühr entprechend zu reduzieren, so dass nicht zuviel eingezogen wird.

*Beispiel 1:* Ein Partner tritt zum 01.12 eines Jahres ein. Der SEPA-Lauf für das zweite Halbjahr ist bereits im September durchgeführt worden, so dass die Aufnahmegebühr für den Partner in normaler Höhe eingetragen wird.

*Beispiel 2:* Ein Partner tritt zum 01.09 eines Jahres ein. Der SEPA-Lauf für das zweite Halbjahr ist noch nicht durchgeführt worden. Wird er durchgeführt, wird für die Monate Juli und August Familienbeitrag eingezogen, obwohl nur ein Einzelbeitrag fällig wäre. Die Aufnahmegebühr muss also um _2 x ( Familienbeitrag - Einzelbeitrag )_ vermindert werden.

====

=== Eintrittsbriefe versenden

Nachdem mehrere Eintrittsgesuche eingegeben worden sind, können für alle Neumitglieder Eintrittsbriefe erstellt werden. Voraussetzung dafür ist, dass es eine Selektion gibt, die alle Mitglieder auflistet, bei denen die Option für den Eintrittsbrief angewählt worden ist:

image:Selektion-Eintrittsbrief.png[]

Dafür wird aus dem Hauptmenu der Punkt kbd:[Briefe] ausgewählt. Als Formular wird `01-KVK-Eintrittsbrief` selektiert und die eben erwähnte Selektion aktiviert.

image:Eintrittsbrief-Verschicken.png[]

Im Abschnitt `Briefverwaltung` muss der Punkt `(4)` angewählt werden.
Nachdem man kbd:[Ok] gedrückt hat, werden die Briefe erstellt und im Email-Ausgangsordner deponiert. Man erkennt jetzt schon unter Punkt `(5)` wieviele Briefe insgesamt erstellt worden sind.

[IMPORTANT,caption=WICHTIG]
====
Es ist essentiell wichtig, dass für alle neu aufgenommenen Mitglieder eine EMail-Adresse vorhanden ist, sonst kommt der gesamte Ablauf des Verschickens der Eintrittsbriefe durcheinander!

Sollte ein Neumitglied keine EMail-Adresse besitzen, darf bei diesem auch die Option `Eintrittsbrief` nicht angewählt sein! Für diesen Mitglied muss man im Nachhinein einen Eintrittsbrief als PDF-Datei erstellen und sie ihm auf dem Postwege zuschicken.
====

Der Ort des Email-Ausgangsordners ist 
```
SPG-Verein\SPG-Daten\mandanten\KVK\emailausgang
```

Hier kann man sich die PDFs noch einmal ansehen, um eventuelle Fehler aufzudecken.

Um die Emails nun auch tatsächlich abzuschicken, muss im Abschnitt `Briefverwaltung` der Punkt `(5)` angewählt werden.

image:Eintrittsbrief-Verschicken-(5).png[]

Dadurch wird das `Betreff`-Feld editiertbar, in das man einen sinnigen Betreff für die Email eingibt. Das Feld für den Nachrichtentext öffnet einen Dialog zum Eingeben oder der Auswahl eines passenden Textes.

== Schlüsselpfand

Erhält ein Mitglied einen Bootshallenschlüssel, so ist dafür das festgelegte Pfand (aktuell 35 Euro) zu entrichten. Der Pfandbetrag wird bei den Mitgliedsdaten im Abschnitt `Abteilungen/Beiträge` in das Feld  `Einmalbetrag(1)` eingetragen. Beim nächsten SEPA-Lauf wird der Betrag dann mit eingezogen und das Feld ist wieder leer.

[IMPORTANT,caption=WICHTIG!]
====
Der Betrag des einzuziehenden Schlüsselpfands ist _immer_ und _nur_ in das Feld  `Einmalbetrag(1)` einzutragen!
====

Über die ausgegebenen Schlüssel ist eine Liste zu führen, die mindestens das Datum der Ausgabe und die Höhe des gezahlten Schlüsselpfandes enthält.

== Austritt eines Mitgliedes
.Ausschnitt aus der Satzung
[NOTE,caption=INFO]
====

§ 3  Verlust der Mitgliedschaft

(1)  Die Mitgliedschaft erlischt durch Austritt oder Ausschluss aus dem Verein. Die Austrittserklärung ist schriftlich an den Vorstand zu richten. 

(2)  Der Austritt ist nach einjähriger Mitgliedschaft unter Einhaltung einer Frist von drei Monaten zum Quartalsende zulässig.
In begründeten Ausnahmefällen kann der Vorstand abweichend einer vorzeitigen Kündigung zustimmen.
====

Nach Erhalt der Austrittserklärung (wir akzeptieren auch E-Mails als Schriftform) wird das Datum der Erklärung in das Feld `Kündigung` eingetragen:

image:Austritt.png[]

Aus dem Eintritts- und dem Kündigungsdatum muss nach den Vorgaben der Satzung das Datum errechnet werden, zu dem der Austritt wirksam wird.

Den Radio-Knopf zum Erstellen eines Austrittsbriefes kann man anhaken, dann kann man später für alle Mitglieder, für die dieser Haken gesetzt ist, automatisch eine Mail verschicken. 

Jetzt kommt der schwierige Teil! 

Wir buchen zweimal im Jahr die Mitgliedsbeiträge ab: Im März für das erste Halbjahr und im September für das zweite. Satzungsgemäß ist ein Austritt zu jedem Quartalsende möglich. Wenn also jemand zum 30. September austritt und der Beitragseinzug noch im September erfolgten, würde für ihn der volle Halbjahresbeitrag abgebucht. Das muss verhindert werden!

Es gibt mehrere Möglichkeiten, dies zu verhindern, die einfachste scheint mir, bei den Mitgliederdaten auf der zweiten Seite im Feld `Erhebung ab` ein Datum einzugeben, das weit in der Zukunft liegt:

image:ErhebungAbMitEinmalbetrag.png[]

Damit wird verhindert, dass der regelmäßige Halbjahresbeitrag fälschlicherweise eingezogen wird.

Den Einzug des Beitrages für das verbleibende Vierteljahr aktiviert man, indem ein entsprechender Betrag im Feld `Einmalbetrag(2)` eingetragen wird (im Bild leider noch falsch dargestellt!) . Dieser ergibt sich aus der Summe der Beitragskomponenten für das entsprechende Mitglied. 

Ist ein Bootshallenschlüssel abgegeben worden, muss der Betrag entsprechend vermindert werden (einen negativen Wert in das Feld `Einmalbetrag(2)` einzutragen, ist leider nicht möglich!).

=== Überprüfung von Verknüpfungen

Wenn die austretende Person jemand ist, von dessen Konto Beiträge für weitere Mitglieder (i.d.R. Familienmitglieder) abgebucht werden, muss für die betroffenen im Verein verbleibenden Mitglieder eine eigene Kontoverbindung angegeben werden. Ggfs. müssen hier die entsprechenden Angaben nachgefragt werden.
Ggfs. muss auch die Beitragsart bei den verbliebenen Mitgliedern angepasst werden.

Die mit dem austretenden Mitglied verknüpften Personen findet man mit einer entsprechenden Selektion, bei der die Mitgliedsnummer (inklusive der führenden Nullen!) angegeben wird:

image:Selektion-Verknuepfung.png[]

=== Austrittsbriefe versenden

Das Verschicken der zugehörigen Austrittsbriefe erfolgt analog wie bei den oben beschriebenen Eintrittsbriefen. Allerdings erfolgen Austritte in der Regel einzeln, so dass einfach das betreffende Mitglied in der Liste selektiert werden kann und man dann aus dem Kontext-Menu (rechter Mausklick) die Option `Einzelbrief` auswählt.

== Überprüfung der Beitragsarten

In unserer 
https://kv-kiel.de/verein/mitgliedsbeitraege/Gebuehrenordnung.pdf[Beitragsordnung]
gibt es einige Kategorien, die an das Alter des betreffenden Mitgliedes gekoppelt sind. Es handelt sich um die Beitragsarten:


[width="100%",options="header",cols="^1,6,3"]
|====================
| Bart | Beschreibung | Name im Programm 
| 5 | Mitglieder unter 18 Jahre,Azubi, Student ... (nach Vorlage einer Bescheinigung) ohne Bootsplatz  |  `JUGENDL U18, o. Boot`
| 6 | Mitglieder unter 18 Jahre,Azubi, Student ... (nach Vorlage einer Bescheinigung) mit Bootsplatz | `JUGENDL U18, mit Boot`
| 7 | Kinder von Familien unter 16 Jahre | `FAM, Partner, Kinder U16`
|====================

Die Zuordnung der Beitragsarten zu den jeweiligen Mitgliedern ist _nicht_ mit deren Geburtsdatum gekoppelt. Auch gibt es keinen Automatismus, bei dem die Vorlage einer Bescheinigung überprüft und dann mit der Beitragsart verrechnet wird. Dieser Abgleich muss rein manuell erledigt werden.

[IMPORTANT,caption=WICHTIG]
====
Das Vorhandensein einer etwaigen Bescheinigung, die eine Ermäßigung rechtfertigt, muss mindestens einmal jährlich geprüft werden. Dazu wird in der ersten Ausgabe der Vereinszeitung jeden Jahres ein Aufruf zur Abgabe einer entsprechenden Studenten- oder sonstigen Bescheinigung aufgerufen. Geht die Bescheinigung nicht rechtzeitig vor dem Termin der Beitragserhebung ein, wird der entsprechend höhere Beitragssatz eingezogen.
====

[IMPORTANT,caption=WICHTIG]
====
Der reine Abgleich mit dem Geburtsdatum (z.B. bei Familienbeiträgen) muss zweimal jährlich jeweils vor dem Beitragseinzug erfolgen.
====

Die Beitragsart `JUGENDL U18, ...` wird leider sowohl für Personen verwendet, die unter 18 Jahre alt sind, wie auch für solche, die aufgrund eines "Mankos" einen reduzierten Beitrag bezahlen.

Um alle Mitglieder zu identifizieren, die den reduzierten "Jugendbeitrag" entrichten, kann man die folgende Selektion benutzen.

image:Bart-Jugendliche.png[]

Bei der Überprüfung muss dann für diejenigen Personen, die bereits älter als 18 Jahre sind, eine Bescheinigung vorgelegt worden sein.

Auch bei der Beitragsart 7 (`FAM, Partner, Kinder U16`) gibt es eine Vermischung von zwei Gründen: Kinder unter 16 und Partner von Mitgliedern, die einen Familienbeitrag entrichten. Man kann sie relativ gut auseinander dividieren, wenn man eine entsprechende Selektion benutzt (Beitragsart=7) und dann aufsteigend nach Geburtsdatum sortiert. Alle Personen, die älter als 16 Jahre sind, müssten dann (Ehe)Partner sein. Diejenigen, für die das nicht gilt, müssen dann (mindestens) in die Beitragsart 5 (`JUGENDL U18, o. Boot`) fallen.

[NOTE,caption=HINWEIS]
====
Für die Zukunft ist zu überlegen, ob man nicht besser separate Beitragsarten für Partner/Kinder bzw. Jugendliche/Ermäßigt einführt. Dafür sollte aber mal eine Gesamtbetrachtung über die Abteilungen/Beiträge durchgeführt werden. Hier schlummert m.E. noch Verbesserungspotenzial.
====


== Beitrags- und Gebührenerhebung

Je zweimal im Jahr werden die Mitgliedsbeiträge sowie die Arbeitsdienstersatzzahlungen eingezogen. Im https://kv-kiel.gitlab.io/kvk-handbuch//04-vorstand/ressorts/mitgliederwartin/#4-termine-bankeinz%C3%BCge[KVK-Handbuch] sind die Termine aufgeführt, zu denen die Beitrags- und Gebühreneinzüge stattfinden. 

=== Mitgliedsbeiträge erheben

[IMPORTANT,caption=WICHTIG]
====

Vor dem Termin der Beitragserhebung ist eine Überprüfung der an das Alter gekoppelten Mitgliedsbeiträge vorzunehmen (siehe oben).

Bevor die Beitragserhebung wirklich durchgeführt wird, ist unbedingt eine Datensicherung vorzunehmen.
====

==== Daten vorbereiten


Bevor irgendwelche Aktionen durchgführt werden, sollten die Daten noch einmal kurz überprüft und angepasst werden. 
Zur besseren Übersicht werden zuerst ein paar hierfür wichtige Spalten mit in die Ansicht aufgenommen. Mit einem Rechtsklick auf eine beliebige Spaltenüberschrift erscheint ein Dialog, in dem man `Spaltenauswahl` selektiert.

image:Spaltenanpassen.png[]

Von den dort angebotenen Spalten zieht man mit der Maus `SEPA-Verw-Zweck(1)`, `EinBetr(1)` und `EinBetr(2)` 
an eine beliebige Position in den Spaltenüberschriften.

image:Spalten-Beitragseinzug.png[]

Wenn man nach diesen Spalten sortiert, bekommt man schnell einen Überblick über die Situation:

* Nur seit der letzten Beitragserhebung neu aufgenommene Mitglieder haben keinen Eintrag in der Spalte `SEPA-Verw-Zweck(1)`
* Die Spalte `EinBetr(1)` wird verabredungsgemäß _nur_ für den Einzug von Schlüsselpfand verwendet. Das Schlüsselpfand beträgt 35 Euro (Stand April 2021). Bei den oben im Bild zu sehenden Werten liegt also einiges im Argen, was vorher korrigiert werden muss.

* Die Spalte `EinBetr(2)` wird dafür verwendet, eine etwaigen Aufnahmegebühr oder Ausgleichsbeträge anzugeben, wenn für ein Mitglied nicht der ganz normale Beitrag für ein komplettes halbes Jahr eingezogen werden soll. Auch hier sollte man sich die Kandidaten und die zugehörigen Beträge einmal kritisch ansehen.

Sind die Daten soweit überprüft, muss nun der Inhalt der Spalte `SEPA-VerwZeck(1)` angepasst werden, da sie noch den Eintrag der letzten Beitragserhebung beinhaltet. Das geschieht am einfachsten, indem man sie mit einer ersten Massenänderung löscht:

image::Massenaenderung1.png[]

und mit einer zweiten Massenänderung einen neuen Wert hineinschreibt (hier vielleicht so etwas wie `Beiträge II/2020`):

image::Massenaenderung2.png[]

==== Beiträge erheben

Das Einziehen der Mitgliedsbeiträge geschieht in mehreren Stufen. Zuerst muss aus dem Hauptmenu `menu:Beiträge[Beitragsverfahren > (1) Beitragsherhebung]` ausgewählt werden:

image:Beitragserhebung1.png[]

Im erscheindenden Fenster sind die rot eingerahmten Elemente zu editieren:

image:Beitragserhebung2.png[]

Bei Punkt (1) ist zuerst der Eintrag `Testlauf` zu wählen, weil man damit problemlos sehen kann, ob alles korrekt abläuft, und man kann diesen Vorgang beliebig oft wiederholen, ohne dass Daten verändert werden. Ein Originallauf verändert massiv den Datenbestand und darf nicht unbedacht ausgeführt werden.

Nach dem Testlauf ist das Ergebnis in der Vorschauf auf Plausibilität zu überprüfen. Erscheint alles korrekt, so kann der Originallauf mit denselben Einstellungen durchgeführt werden.

==== SEPA-Datei erstellen

Nach erfolgreichem Originallauf für die Beitragserhebung erscheint automatisch ein Dialog-Fenster, das danach fragt, ob die SEPA-Datei erstellt werden soll. Hier kann man also direkt die Datei erstellen.

Die Datei sollte an eine wohldefinierte Stelle im Dateisystem abgelegt werden, da man sie später mittels Online-Banking zur Sparkasse übertragen muss.

==== Bankeinzug durchführen

[WARNING,caption=NOCH TUN]
====
Hier muss der Ablauf des Online-Banking-Vorgangs beschrieben werden!
====

=== Einzug der Arbeitsdienstersatzzahlungen

[IMPORTANT,caption=WICHTIG]
====
Bevor der Einzug der Arbeitsdienstersatzzahlungen wirklich durchgeführt wird, ist unbedingt eine Datensicherung vorzunehmen.
====

Grob skizziert verläuft der Einzug der Arbeitsdienstersatzzahlungen folgendermaßen:

* das Arbeitsdienstabrechnungsprogramm https://baltic-mh.github.io/ADHelper/html/usermanual.html[ADHelper] erzeugt mit Abschluss des Abrechnungszeitraumes eine Datei `ZuZahlendeStunden.csv`, in der eine Liste der Mitglieder enthalten ist, die nicht die notwendige Anzahl von Arbeitsdienststunden abgeleistet haben.
* die Datei mit den Angaben über die zu zahlenden Stunden in das Vereinsverwaltungsprogramm `SPG-Verein` importieren
* Beitragslauf durchgeführen, bei dem nur die Beträge im Feld `Einmalbetrag(1)` eingezogen werden
* eine SEPA-Datei erzeugen
* die SEPA-Datei zur Bank übertragen.

==== Bestimmung der zu zahlenden Beträge

Die Datei `ZuZahlendeStunden.csv` befindet sich im Unterverzeichnis 

====
_#<Installationsverzeichnis von ADHelper>#_\Arbeitsdienstabrechnungen\Daten\ ##__<Aktuelles Abrechnungshalbjahr>__##
====

Also beispielsweise:
====
##d:\KVK\ADHelper##\Arbeitsdienstabrechnungen\Daten\ #2020-07-01 - 2020-12-31#
====

[NOTE,caption=HINWEIS]
====
Die oben _#farbig#_ hinterlegen Anteile müssen entsprechend den lokalen und aktuellen Gegebenheiten angepasst werden.
====

Sie beinhaltet vier Spalten mit den Überschriften:

* `Mitglieds_Nr`
* `Nachname`
* `Zu zahl. Stunden`
* `Zu zahl. Betrag`

[IMPORTANT,caption=WICHTIG]
====
Die Einträge für die Spalten sind durch ein Semikolon (;) voneinander getrennt. Das muss unbedingt so erhalten bleiben, weil sonst das Programm `SPGVerein` damit nicht klar kommt. 

Wenn man die Datei mit einem Tabellenkalkulationsprogramm (Excel, Libre-Office, etc.) öffnet, kann es sein, dass dieses Veränderungen an den Daten vornimmt (beliebt ist die Ersetzung aller Semicolions durch Kommata!), darf die Datei auf gar keinen Fall aus diesem Programm heraus gespeichert werden!
====

[WARNING,caption=ACHTUNG!]
====
In den unten folgenden Abbildungen ist die vierte Spalte mit `Euro` bezeichnet. Das stammt noch aus der Zeit, als das Programm `ADHelper` diese Spalte noch nicht selbständig gefüllt hat. Für den Gang der Dinge ist das aber unerheblich.
====

[IMPORTANT,caption=WICHTIG]
====
Es ist sicherzustellen, dass das Verhältnis aus den Zahlenwerten in der Spalte `Zu zahl. Betrag` und `Zu zahl. Stunden` der aktuelle Stundensatz ist. 
====

==== Daten im Mitgliederverwaltungsprogramm bereinigen

Nun muss das Mitgliederverwaltungsprogramm gestartet und als erstes eine Datensicherung durchgeführt werden, wenn das nicht bereits geschehen ist.

Die Beträge, die von den einzelnen Mitglieder als Ersatzzahlung für nicht geleistete Arbeitsdienste eingezogen werden sollen, werden in das Feld `Einmalbetrag (1)` (oder `EinBetr (1)`) übertragen.
Daher ist zu überprüfen, ob aktuell bereits Einträge existieren, bei denen das Feld `EinBetr(1)` Werte enthält. Das ist z.B. der Fall, wenn hier schon einzuziehendes Schlüsselpfand eingetragen ist.

Die vorhandenen Einträge müssen entfernt werden, weil sie sonst vom Import überschrieben werden. Die Überprüfung geschieht am einfachsten, indem man die Spalte `EinBetr(1)` sichtbar gemacht und danach sortiert wird. Dann scrollt man nach ganz unten und nach ganz oben. Wenn an beiden Enden nur Einträge mit dem Wert `0,0` zu sehen sind, ist alles gut. Alle anderen Beiträge müssen jetzt entfernt und später wieder eingefügt werden.

[IMPORTANT,caption=WICHTIG!]
====
Es ist wichtig, sich die in der Spalte `EinBetr(1)` vorhandenen Daten vor dem Löschen zu merken, oder, falls es zu viele sind, kurz zu exportieren. Sie müssen nach der Durchführung der Beitragserhebung wiederhergestellt werden!
====

Zusätzlich muss noch die Spalte `SEPA-VerwZeck(1)` angepasst werden. Das geschieht am einfachsten, indem man sie mit einer ersten Massenänderung löscht:

image::Massenaenderung1.png[]

und mit einer zweiten Massenänderung einen neuen Wert hineinschreibt:

image::Massenaenderung2.png[]

==== Import der Datei `ZuZahlendeStunden.csv`

Ist sichergestellt, dass die Spalte `EinBetr(1)` leer ist, ist aus dem Menu `menu:Extras[Daten importieren]` auszuwählen. Hier muss die eben erweiterte Datei angegeben, der Haken bei `Feldnamen in der ersten Zeile` gesetzt und die Option `Bestand ändern ...` ausgewählt werden.

image::ZuZahlendeStunden_Import1.png[Import1]

Im Reiter `Zuordnungen` müssen die Spalten der `.csv`-Datei auf die entsprechenden Spalten des Mitgliederverwaltungsprogramms abgebildet werden. Dazu muss jeweils eine Spalte auf der linken Seite und die zugehörige auf der rechten Seite selektiert und danach `hinzufügen` gedrückt werden.

Es müssen nur die beiden Spalten `Mitglieds_Nr` und `Zu zahl. Betrag` (im Bild: `Euro`) zugeordnet werden.

image::ZuZahlendeStunden_Import2.png[Import2]

==== Betragserhebung durchführen

Wie das Einziehen der Mitgliedsbeiträge geschieht auch das Einziehen der Arbeitsdienstersatzzahlungen in mehreren Stufen. Zuerst muss aus dem Hauptmenu `menu:Beiträge[Beitragsverfahren > (1) Beitragsherhebung]` ausgewählt werden:

image::Beitragserhebung1.png[Menue Beitragserhebung]

Es ist auch hier zuerst wieder ein Testlauf vorzunehmen. Bei den Einstellungen sind alle Haken bei den Zahlungsterminen zu entfernen, damit keine normalen Beiträge erhoben werden. Lediglich beim Feld `Einmalbetrag (1)` ist ein Haken zu setzen und ein sinnfälliger Text einzutragen.

image::Beitragserhebung-Arbeitsdienst.png[Beitragserhebung-Arbeitsdienst]

Nach dem Testlauf ist das Ergebnis in der Vorschauf auf Plausibilität zu überprüfen. Erscheint alles korrekt, so kann der Originallauf mit denselben Einstellungen durchgeführt werden.

==== SEPA-Datei erstellen

Nach erfolgreichem Originallauf für die Beitragserhebung erscheint automatisch ein Dialog-Fenster, das danach fragt, ob die SEPA-Datei erstellt werden soll. Hier kann man also direkt die Datei erstellen.

Die Datei sollte an eine wohldefinierte Stelle im Dateisystem abgelegt werden, da man sie später mittels Online-Banking zur Sparkasse übertragen muss.

==== Bankeinzug durchführen

[WARNING,caption=NOCH TUN]
====
Hier muss der Ablauf des Online-Banking-Vorgangs beschrieben werden!
====

==== Entfernte Einträge wieder einfügen

[IMPORTANT,caption=WICHTIG!]
====
Die Einträge im Feld `EinBetr(1)`, die vorher entfernt worden sind, müssen nun wiederhergestellt werden!
====

== Aktualisierung der Daten für `ADHelper`-Programm

Das Programm für die Berechnung der Arbeitsdienstersatzzahlungen `ADHelper` benötigt eine Liste der Mitglieder mit den dazugehörigen aktuellen Daten. Diese Liste wird aus dem Vereinsverwaltungsprogramm `SPG-Verein` als `.csv`-Datei erzeugt und diese schließlich in `ADHelper` importiert.

=== Export

Über den Punkt `menu:Extras[Daten exportieren]` aus dem  Hauptmenu gelangt man zum folgenden Dialog:

image::Export-ADHelper.png[]

Dort ist im Feld `Auswahl Vorlage` die bestehende Vorlage `AD Export` zu selektieren.

[TIP,caption=TIPP]
====
Existiert diese Vorlage noch nicht, sollte sie angelegt werden (sonst muss man jedes mal diesen Dialog wieder manuell ausfüllen). 

Das Anlegen geschieht, indem man oben im Dialog einmal `Neue Vorlage` anklickt, dann den gesamten Dialog wie abgebildet ausfüllt und am Schluss `Vorlage speichern` drückt.
====

Ist alles wie angegeben konfiguriert, wird nach Drücken von `Export durchführen` eine neue Datei geschrieben. Den Ort, wo man diese Datei abgelegt hat, sollte man sich merken ;-)

=== Import 

Um die neuen Daten in `ADHelper` zu importieren, muss das Programm gestartet werden.

[IMPORTANT,caption=WICHTIG]
====
Es muss sichergestellt sein, dass das Programm mit der Rolle `MITGLIEDERWART` gestartet wird. Das kann man überprüfen, indem man den Dialog für die Benutzerdaten aus dem Hauptmenu über `menu:Aktionen[Benutzerdaten...]` aufruft. 

image:ADHelper-Benutzerdaten.png[]

Wenn hier nicht `MITGLIEDERWART` selektiert ist, muss man das korrigieren und das Programm neu starten. Einmal eingestellt, bleibt diese Einstellung aber erhalten.
====

Den Import startet man über den Knopf kbd:[Daten hochladen] ganz rechts unten in der Ecke. Es öffnet sich ein Dateiauswahl-Dialog, mit dem man die vorhin exportierte Datei selektiert.

image:UploadBasisdaten-Dateiauswahl.png[]

Dort wird im Feld `Suchen in:` angezeigt, welches Verzeichnis das Programm gerade im Fokus hat. Wenn das nicht dasjenige ist, in das man die Datei exportiert hat, erhält man durch einen Mausklick auf das schwarze Dreieck in derselben Zeile den gesamten Dateibaum des Rechners angezeigt. Ggfs. muss man den Scrollbalken noch etwas nach oben schieben, um das gewünschte Laufwerk in den Sichtbereich zu bekommen.

Hat man das Export-Verzeichnis gefunden und geöffnet, muss man darin die vorhin exportierte Datei selektieren und schließlich durch Drücken des kbd:[Öffnen]-Knopfes von `ADHelper` einlesen lassen.

image:UploadBasisdaten-Dateiausgewaehlt.png[]

Beim Einlesen wird die Datei vom Programm auf Konsistenz überprüft. Genügt sie nicht den formalen Anforderungen, wird sie nicht auf den Server hochgeladen. Entweder gibt die angezeigte Fehlermeldung einen Hinweis auf das bestehende Problem und man kann es eigenständig beheben, oder man muss den Entwickler von `ADHelper` um Unterstützung bitten.

Im untenstehenden Bild ist ein Beispiel aus dem realen Leben zu sehen: hier stand beim Mitglied mit der Mitgliedsnummer 10053 im Feld `Verknüpfung` (RefID) keine Zahl sondern der Text `00Infopost`:

image:ADHelper-ImportError.png[]

Wenn keine Fehler erkannt werden, wird die Datei auf den Server transferriert und die Berechnung der Arbeitsdienstersatzzahlungen kann auf den neuesten Daten aufsetzen.



== Verbandsmeldungen

=== LSV

=== LKV

== Jahresabschluss

Der Jahresabschluss dient dazu, den Datenbestand von unnötigem Ballast zu befreien.

=== Daten sichern

Die Daten des aktuellen Jahres (hier 2020) werden in einem Ordner `SPG-Daten\Jahresabschlüsse\2020` hinterlegt. In diesen Ordner wird zuerst eine normale Datensicherung gemacht (`menu:Extras[Datensicherung > Datenbestand sichern]`). Der Dateiname soll das Suffix `-VorPeriodenwechsel` erhalten.

Danach werden alle relevanten Listen in denselben Ordner gespeichert. Das sind die in den nächsten vier Abschnitten beschriebenen Daten.

==== Mitgliederkonten

Im linken Bereich ist aus dem Tabulator `Sonstige Listen` der Punkt `Mitgliedskonto (Liste)` aufzurufen:

image::ListenAusgabe-Mitgliederkonten1.png[]

Im erscheinenden Dialogfenster sind die folgenden Einstellungen vorzunehmen:

* Ausgabe Stammdaten erweitert
* Ausgabe Kontosaldo = 0
* pro Seite ein Mitglied

* Ausgabeart - alpabetisch

image::ListenAusgabe-Mitgliederkonten2.png[]

Nach Drücken der kbd:[OK]-Taste ist in den Ausgabeeinstellungen das PDF-Format zu wählen.

image::ListenAusgabe-Mitgliederkonten3.png[]

Nach Betätigen des kbd:[Starten]-Knopfes ist im Dialog der oben genannte Ausgabeordner zu wählen und als Dateiname `Mitgliederkonten.pdf`  anzugeben.

==== Saldenliste

Hier werden alle Mitglieder ausgegeben, die kein ausgeglichenes Mitgliederkonto haben.

Im linken Bereich ist aus dem Tabulator `Sonstige Listen` der Punkt `Saldenliste` aufzurufen:

image::ListenAusgabe-Salden1.png[]

Im erscheinenden Dialogfenster ist sicherzustellen, dass die Option `Ausgabe Kontensaldo = 0,00` _nicht_ angehakt ist!

image::ListenAusgabe-Salden2.png[]

Auch hier erfolgt die Ausgabe wieder in das Verzeichnis für den Jahresabschluss diesen Jahres in eine PDF-Datei -- diesmal mit dem Namen `Saldenliste.pdf`.

==== Änderungsprotokoll

Im linken Bereich ist aus dem Tabulator `Sonstige Listen` der Punkt `Änderungsprotokoll` aufzurufen:

image::ListenAusgabe-Aenderungsprotokoll1.png[]

In der erscheinenden Liste ist das Druckersymbol anzuklicken:

image::ListenAusgabe-Aenderungsprotokoll2.png[]

Bei den Optionen ist lediglich die Ausgabeart auf `alphabetisch` zu stellen:

image::ListenAusgabe-Aenderungsprotokoll3.png[]

Auch hier erfolgt die Ausgabe wieder in das Verzeichnis für den Jahresabschluss diesen Jahres in eine PDF-Datei -- diesmal mit dem Namen `Änderungsprotokoll.pdf`.


==== Statistik

Im linken Bereich ist aus dem Tabulator `Statistik` der Punkt `Statistik (Liste)` aufzurufen:

image::ListenAusgabe-Statistik1.png[]

Die Tabelle der Altersgruppierungen ist wie auf der Abbildung zu sehen einzugeben (mit dem Haken bei `Altersgruppen speichern?` stellt man sicher, dass man diese Eingabe nur ein einziges Mal machen muss):

image::ListenAusgabe-Statistik2.png[]

Auch hier erfolgt die Ausgabe wieder in das Verzeichnis für den Jahresabschluss diesen Jahres in eine PDF-Datei -- diesmal mit dem Namen `Statistik.pdf`.

=== Periodenwechsel durchführen

Aus dem Hauptmenu ist der Punkt `menu:Beiträge[Jahr.-/Periodenwechsel]`) aufzurufen:

image::Periodenwechsel1.png[]

Bei den Optionen ist folgendes auszuwählen:

* Vortrag-Vorjahr neu aufbauen
* Umsatzdaten - alle löschen
* Zahlungsdaten - alle löschen
* Änderungsdaten - alle löschen
* ausgetretene Mitglieder - alle löschen

image::Periodenwechsel2.png[]

Nach Drücken des kbd:[OK]-Knopfes wird durch die Nachfrage, ob man das wirklich ernst meint, signalisiert, dass hier eine massive Datenänderung vorgenommen wird. Man kann aber ganz unbesorgt sein, weil ja gerade vorher der gesamte Datenbestand gesichert worden ist.

Nachdem der Periodenwechsel tatsächlich durchgeführt worden ist, wird abermals eine Datensicherung in das oben genannte Verzeichnis gemacht.  Der Dateiname soll das Suffix `-NachPeriodenwechsel` erhalten.


[NOTE,caption=HINWEIS]
====
Nach Abschluss des Periodenwechsels müssen im Verzeichnis für den betreffenden Jahresabschluss folgende sechs Dateien zu finden sein:

image::Periodenwechsel-Dateien.png[]

====

[IMPORTANT,caption=WICHTIG!]
====
Nach dem Periodenwechsel soll nichts mehr in das Verzeichnis dieses Jahresabschlusses geschrieben werden!
====
