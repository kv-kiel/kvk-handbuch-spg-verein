= KVK-Handbuch `SPG-Verein`
Mathias-H. Weber <mhw@teambaltic.de>
v1.0, 06. Juni 2020
:doctype: article
:encoding: utf-8
:lang: de
//:toc: left
:toclevels: 4
:toc-title: Inhaltsverzeichnis
:last-update-label: Erstellt mit Asciidoctor v{asciidoctor-version} : Zuletzt geändert:
// Ohne dem haben die "Admonition"-Blocks keine Icons!
:icons: font
:numbered:
:source-highlighter: highlightjs
// Deutsche Überschriften:
:figure-caption: Abbildung
:table-caption: Tabelle
:chapter-label: Kapitel
//:example-caption!:
// Jeder Abschnitt bekommt automatisch einen Anker:
:sectanchors:
// Makro "kbd:" aktivieren:
:experimental:
:imagesdir: images

// Antora Attribute:
:page-toctitle: Inhalt

Zur Verwaltung der Mitgliederdaten wird in der http://www.kv-kiel.de[Kanu-Vereinigung Kiel] das Programm 
https://spg-direkt.de/[`SPG-Verein`] eingesetzt, das wir freundlicherweise von der https://www.foerde-sparkasse.de/de/home.html[Sparkasse] unentgeldlich gestellt bekommen.

Mittlerweile ist eine sehr schöne Online-Dokumentation des Programmes selbst im Internet verfügbar: https://spg-direkt.de/spgverein_hilfesystem_4.3/#[SPG Verein 4.3 Handbuch].

In dieser Dokumentation wird nur die Bedienung des Programmes beschrieben mit Schwerpunkt darauf, wie es in unserem Verein benutzt wird.
Die einzelnen Aufgaben, die die Mitgliederwartin im Verein zu betreuen hat, sind im https://kv-kiel.gitlab.io/kvk-handbuch/[KVK-Handbuch] dokumentiert
